##Test Task
Description:  
- This is test scenario for the https://www.vpnmentor.com/tools/ipinfo/ page using page object pattern.
  
Instruments used:  
- Selenium WebDriver  
- pytest
  
Author:  
- Rybintseva Tatyana

Issue faced:  
- Google recaptcha. See [screenshot](https://i.imgur.com/ELvEldk.png)  

Requirements:  
- Python 3  
- See [requirements file](requirements.txt)  