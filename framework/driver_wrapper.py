from selenium import webdriver

from webdriver_manager.chrome import ChromeDriverManager


class Driver:
    @staticmethod
    def get_driver():
        driver = webdriver.Chrome(ChromeDriverManager().install())
        return Driver.add_driver_settings(driver)

    @staticmethod
    def add_driver_settings(driver):
        driver.implicitly_wait(5)
        driver.set_page_load_timeout(20)
        driver.set_window_size(1366, 768)
        return driver
