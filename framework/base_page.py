from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class BasePage:
    timeout_sec = 5

    def __init__(self, driver):
        self.driver = driver

    # Open page by full url
    def go_to_exact_url(self, url=""):
        self.driver.get(url)

    # Find element on page
    def find_element(self, *locator):
        return self.driver.find_element(*locator)

    # Click on web element
    def click(self, *locator):
        self.wait_until_element_to_be_clickable(locator)
        self.driver.find_element(*locator).click()

    # Wait until element will be clickable on the page
    def wait_until_element_to_be_clickable(self, *locator, timeout=timeout_sec):
        try:
            WebDriverWait(self.driver, timeout).until(EC.element_to_be_clickable(*locator))
        except TimeoutException:
            raise AssertionError('Element missed. It takes more than {} sec to load an element'.format(timeout))

    # Check that web element is present on the page
    def is_element_present(self, *locator):
        try:
            self.find_element(*locator)
        except NoSuchElementException:
            return False
        return True

    # Get text from web element
    def get_text(self, *locator):
        if WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(locator)):
            element = self.find_element(*locator)
            return element.text
        else:
            return None

    def type(self, text, *locator):
        element = self.driver.find_element(*locator)
        element.clear()
        element.send_keys(text)

    def wait_until_invisibility_of_element_located(self, locator, timeout=timeout_sec):
        try:
            WebDriverWait(self.driver, timeout).until(EC.invisibility_of_element_located(locator))
            self.find_element(*locator)
        except NoSuchElementException:
            return False
        return True
