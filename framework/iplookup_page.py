from framework.locators import *
from framework.dictionary import *
from framework.base_page import BasePage


class IPLookUpPage(BasePage):

    # Check elements displaying on the page
    def check_ipinfo_page_elements(self):
        self.pre_step()
        return self.is_element_present(*IPInfoLocators.maim_logo) and \
            self.is_element_present(*IPInfoLocators.ip_address) and \
            self.is_element_present(*IPInfoLocators.footer_logo)

    # Check current IP and data
    def check_current_ip(self):
        return self.get_text(*IPInfoLocators.ip_country) == DEFAULT_COUNTRY and \
            self.get_text(*IPInfoLocators.ip_state) == DEFAULT_STATE and \
            self.get_text(*IPInfoLocators.ip_city) == DEFAULT_CITY and \
            self.get_text(*IPInfoLocators.ip_address) == DEFAULT_IP_ADDRESS

    # Check any other IP
    def check_google_ip(self):
        self.pre_step()
        self.type(GOOGLE_IP_ADDRESS, *IPInfoLocators.iplookup_input)
        self.click(*IPInfoLocators.iplookup_button)
        return self.get_text(*IPInfoLocators.ip_country) == GOOGLE_COUNTRY and \
            self.get_text(*IPInfoLocators.ip_address) == GOOGLE_IP_ADDRESS

    # Check any wrong IP
    def check_wrong_ip(self):
        self.pre_step()
        self.type(WRONG_IP_ADDRESS, *IPInfoLocators.iplookup_input)
        self.click(*IPInfoLocators.iplookup_button)
        return self.get_text(*IPInfoLocators.wrong_ip_info) == WRONG_IP_INFO

    # Check language switcher functionality
    def check_language_switcher(self):
        self.pre_step()
        self.click(*IPInfoLocators.language_switcher)
        self.click(*IPInfoLocators.fr_language)
        self.wait_until_invisibility_of_element_located(IPInfoLocators.preloader)
        return self.get_text(*IPInfoLocators.ip_label) == FRENCH_IP_LABEL and \
            self.get_text(*IPInfoLocators.country_label) == FRENCH_COUNTRY_LABEL and \
            self.get_text(*IPInfoLocators.state_label) == FRENCH_STATE_LABEL and \
            self.get_text(*IPInfoLocators.city_label) == FRENCH_CITY_LABEL

    # Open the Ipinfo page and wait until preloader disappears
    def pre_step(self):
        self.go_to_exact_url(BASE_URL)
        self.wait_until_invisibility_of_element_located(IPInfoLocators.preloader)
