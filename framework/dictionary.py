BASE_URL = "https://www.vpnmentor.com/tools/ipinfo/"

# Put here your current data
DEFAULT_COUNTRY = "Ukraine"
DEFAULT_STATE = "Kyiv City"
DEFAULT_CITY = "Kyiv"
# Put here your current IP address
DEFAULT_IP_ADDRESS = ""

GOOGLE_COUNTRY = "United States"
GOOGLE_IP_ADDRESS = "8.8.8.8"
WRONG_IP_ADDRESS = "123456"
WRONG_IP_INFO = "No Information Found"
FRENCH_IP_LABEL = "Voici votre adresse IP:"
FRENCH_COUNTRY_LABEL = "Pays"
FRENCH_STATE_LABEL = "Etat/Province"
FRENCH_CITY_LABEL = "Ville"
