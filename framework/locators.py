from selenium.webdriver.common.by import By


class IPInfoLocators:
    maim_logo = (By.XPATH, "//a[@id='main_logo']")
    footer_logo = (By.XPATH, "//div[@class='logo']/a[contains(.,'vpnMentor')]")
    iplookup_input = (By.XPATH, "//input[@id='iplookup']")
    iplookup_button = (By.XPATH, "//button[@id='cmdSubmit']")
    ip_address = (By.XPATH, "//p[@id='dIP']")
    ip_country = (By.XPATH, "//strong[@id='dCountry']")
    ip_state = (By.XPATH, "//strong[@id='dState']")
    ip_city = (By.XPATH, "//strong[@id='dCity']")
    wrong_ip_info = (By.XPATH, "//strong[text()='No Information Found']")
    language_switcher = (By.XPATH, "(//a[@class='dropdown-toggle'])[last()]")
    fr_language = (By.XPATH, "(//a[@title='Français'])[last()]")
    ip_label = (By.XPATH, "//div/p[@class='ip-label']")
    country_label = (By.XPATH, "//div[@id='dCountry_wrap']/span")
    state_label = (By.XPATH, "//div[@id='dState_wrap']/span")
    city_label = (By.XPATH, "//div[@id='dCity_wrap']/span")
    preloader = (By.XPATH, "//div[@id='preloader']")
