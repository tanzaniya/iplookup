import unittest

from framework.driver_wrapper import *


class TestBase(unittest.TestCase):
    driver = None
    properties = False

    @classmethod
    def setUpClass(cls):
        cls.driver = Driver.get_driver()

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
