from tests.ui_tests.test_base import TestBase
from framework.iplookup_page import *


class TestIPLookUp(TestBase):
    @classmethod
    def setUpClass(cls):
        super(TestIPLookUp, cls).setUpClass()
        cls.ip_look_up_page = IPLookUpPage(cls.driver)

    def test_01_go_to_ipinfo_page(self):
        self.assertTrue(self.ip_look_up_page.check_ipinfo_page_elements(), "The IP Look Up page is broken")

    def test_02_check_current_ip(self):
        self.assertTrue(self.ip_look_up_page.check_current_ip(), "The data on the page is wrong")

    def test_03_check_google_ip(self):
        self.assertTrue(self.ip_look_up_page.check_google_ip(), "The GOOGLE data is wrong")

    def test_04_check_wrong_ip(self):
        self.assertTrue(self.ip_look_up_page.check_wrong_ip(), "The wrong IP message isn't displayed")

    def test_05_check_language_switcher(self):
        self.assertTrue(self.ip_look_up_page.check_language_switcher(), "The language switcher doesn't work")
